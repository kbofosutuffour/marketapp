Django==5.0
django-cors-headers==4.3.1
django-environ==0.11.2
djangorestframework==3.14.0
Pillow==10.1.0
