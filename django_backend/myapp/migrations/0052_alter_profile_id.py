# Generated by Django 4.2.4 on 2023-11-25 19:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0051_alter_room_buyer_profile_picture_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='id',
            field=models.IntegerField(default=None, primary_key=True, serialize=False),
        ),
    ]
